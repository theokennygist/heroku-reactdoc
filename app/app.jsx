import React from 'react'
import ReactDOM from 'react-dom'
import Bootstrap from 'bootstrap.css'
import Index from 'index.scss'
import {Route, Router, IndexRoute, hashHistory} from 'react-router'

import Main from 'Main'
import Packages from 'Packages'
import ReactTemplate from 'ReactTemplate'
import GettingStarted from 'GettingStarted'
import Configs from 'Configs'
import Structure from 'Structure'

export default class App extends React.Component {
	render() {
		return (
			<div class="container">
				<Main />
			</div>
		)
	}
}

const appDOM = document.getElementById('app')
//render() - a function from ReactDOM package
ReactDOM.render(
			<Router history={hashHistory}>
				<Route path="/" component={Main}>
					<Route path="/GettingStarted" component={GettingStarted} />
					<Route path="/packages" component={Packages} />
					<Route path="/configs" component={Configs} />
					<Route path="/structure" component={Structure} />					
					<IndexRoute component={ReactTemplate} />
				</Route>
			</Router>, 
			appDOM
);