import React from 'react'

export default class Settings extends React.Component {
	render() {
		return (
			<div class="container">				
				<section class="body-heading">
					<h1>webpack.config.js</h1>
				</section>
				<section class="body-section">					
					<p>A file which contains configurations for the webpack</p>
				<ul>
					<li><h3>entry</h3></li>
					<p>
						It contains a setting for webpack to process the code 
					</p>
					<li><h3>output</h3></li>
					<p>
						It specifies the result of bundle files
					</p>
					<li><h3>modulesDirectories</h3></li>
					<p>
						A setting to specify folder paths of import files 
					</p>
					<li><h3>alias</h3></li>
					<p>
						A setting to specify individual file paths of import files 
					</p>
					<li><h3>loaders</h3></li>
					<p>
						A setting to specify each loaders for webpack
					</p>
				</ul>
				</section>
				<section class="body-heading">
					<h1>karma.conf.js</h1>
				</section>
				<section class="body-section">
					<p>A file which contains configurations for the karma</p>
				<ul>
					<li><h3>browsers</h3></li>
					<p>
						A setting to specify which browsers to run the test
					</p>
					<li><h3>frameworks</h3></li>
					<p>
						A setting to specify test frameworks to use
					</p>
					<li><h3>files</h3></li>
					<p>
						A setting to specify test files and path 
					</p>
					<li><h3>reporters</h3></li>
					<p>
						A setting to specify test report 
					</p>
				</ul>
				</section>
			</div>
		)
	}
}