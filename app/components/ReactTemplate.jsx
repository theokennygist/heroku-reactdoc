import React from 'react'
import {Link, IndexLink} from 'react-router'

import ReactIcon from 'react-icon.png'
import MochaIcon from 'mocha-icon.svg'
import SassIcon from 'sass-icon.png'
import ReduxIcon from 'redux-icon.png'

export default class Reacttemplate extends React.Component {
	render() {
		return (
			<div class="container">
				<section class="container text-center body-section">					
					<h1 class="blue-color-heading">Heroku-React</h1>
					<h3>
						<p>An open-source NodeJS packages</p> 
						<p>It helps to build React Application in short time</p>
						<p>Focus on the apps not on the setting</p>
						<Link to="gettingstarted" class="btn btn-primary">Getting Started</Link>	
					</h3>
				</section>
				
				<div class="container">
					<div class="row">
						<div class="col-md-3 text-center">
							<img src={ReactIcon} class="loggo-icon" />
							<h2 class="blue-color-heading">Routing</h2>
							<p class="p-text"><b>React Router</b><br />
								Declarative routing package for React								
							</p>
							<a class="btn btn-default" href="https://www.npmjs.com/package/react-router">
								Learn more
							</a>							
						</div>
						<div class="col-md-3 text-center">
							<img src={MochaIcon} class="loggo-icon" />
							<h2 class="blue-color-heading">Testing</h2>
							<p  class="p-text"><b>Mocha Testing</b><br />
								Simple and flexible javascript framework
							</p>
							<a class="btn btn-default" href="https://mochajs.org">
								Learn more
							</a>							
						</div>
						<div class="col-md-3 text-center">
							<img src={ReduxIcon} class="loggo-icon" />
							<h2 class="blue-color-heading">Connect</h2>
							<p  class="p-text"><b>React-Redux</b><br />
								Package to bind React with Redux
							</p>
							<a class="btn btn-default" href="https://github.com/reactjs/react-redux">
								Learn more
							</a>							
						</div>
						<div class="col-md-3 text-center">
							<img src={SassIcon} class="loggo-icon" />
							<h2 class="blue-color-heading">Stylesheets</h2>
							<p class="p-text"><b>Sass</b><br />
								Mature, powerful, and stable CSS extension
							</p>
							<a class="btn btn-default" href="http://sass-lang.com">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		)
	}
}