import React from 'react'

import ProjectStructure from 'project-structure.png'
import RepoStructure from 'repo-structure.png'

export default class Structure extends React.Component {
	render() {
		return (
			<div class="container">
				<section class="body-heading">
					<h3>Heroku-React component structure</h3>
				</section>
				<section class="body-section">
					<p class="alert alert-info">NOTE: Heroku-React is structured like image below:</p>
					<img src={ProjectStructure} class="project-structure center-block"/>
				</section>
				<section class="body-heading">
					<h3>Heroku-React folder structure</h3>
				</section>
				<section class="body-section">
					<p class="alert alert-info">NOTE: Heroku-React folder is structured: </p>
					<img src={RepoStructure} class="project-structure center-block"/>
				</section>
			</div>
		)
	}
}