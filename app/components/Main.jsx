import React from 'react'

import Navigation from 'navigation'
import ReactTemplate from 'ReactTemplate'

export default class Main extends React.Component {
	
	render() {
		return (
			<div>				
				<Navigation />	
				{this.props.children}
			</div>
		)
	}
}