import React from 'react'

export default class Packages extends React.Component {
	render() {
		return (
			<div class="container">				
				<section class="body-heading">
					<h3>React</h3>
				</section>
				<section class="body-section">					
					<pre>
						<ul>
						
							<li>react</li>
							<li>react-dom</li>

						</ul>
					</pre>
					<p class="alert alert-info">NOTE: Packages to use React</p>
				</section>
				<section class="body-heading">
					<h3>Webpack</h3>
				</section>
				<section class="body-section">
					<pre>
						<ul>
							<li>webpack</li>
						</ul>
					</pre>
					<p class="alert alert-info">NOTE: A package to bundle project files</p>
				</section>
				<section class="body-heading">
					<h3>Babel</h3>
				</section>
				<section class="body-section">
					<pre>
						<ul>
							<li>babel-core</li>
							<li>babel-loader</li>
							<li>babel-preset-es2015</li>
							<li>babel-preset-react</li>
						</ul>
					</pre>
					<p class="alert alert-info">
						NOTE: A packages dependencies and a package to use ES2015 javascript <br />
						webpack.config.js contains setting to bundle javascript
					</p>
				</section>
				<section class="body-heading">
					<h3>HTML Attribute</h3>
				</section>
				<section class="body-section">
						<pre>
							<ul>
								<li>babel-plugin-react-html-attrs</li>
							</ul>
						</pre>
						<p class="alert alert-info">NOTE: HTML "class" attribute is used in React.
						Hence, developers need to use HTML "classname" attribute instead. <br />
						However, this package allows us to use "class" attribute within HTML file.
						</p>
				</section>
				<section class="body-heading">
					<h3>Bootstrap</h3>
				</section>
				<section class="body-section">
					<pre>
						<ul>
							<li>bootstrap</li>
							<li>jquery</li>
						</ul>
					</pre>
					<p class="alert alert-info">NOTE: JQuery is needed for several Bootstrap features 
						such as a Bootstrap modal page.<br />						
					</p>
					<p class="alert alert-warning">
						JQuery is optional. 
					</p>
				</section>
				<section class="body-heading">
					<h3>Loaders</h3>
				</section>
				<section class="body-section">
					<pre>
						<ul>
							<li>url-loader</li>
							<li>style-loader</li>
							<li>script-loader</li>						
							<li>file-loader</li>
							<li>css-loader</li>
						</ul>
					</pre>
					<p class="alert alert-info">
						NOTE: webpack uses loaders to load packages.
					</p>
					<p class="alert alert-warning">
						package.json - contains list of the HerokuReact packages. 
					</p>
				</section>
				<section class="body-heading">
					<h3>Testing</h3>
				</section>
				<section class="body-section">
					<pre>
						<ul>
							<li>karma</li>
							<li>karma-chrome-launcher</li>
							<li>karma-mocha</li>
							<li>karma-mocha-reporter</li>
							<li>karma-sourcemap-loader</li>
							<li>karma-webpack</li>
							<li>react-addons-test-utils</li>
							<li>react</li>
							<li>react-dom</li>
						</ul>
					</pre>
					<p class="alert alert-info">NOTE: karma.conf.js contains testing configuration setting.</p>
				</section>															
			</div>
		)
	}
}