import React from 'react'

import ProjectStructure from 'project-structure.png'

export default class GettingStarted extends React.Component {
	render() {
		return (
			<div class="container">							
				<section class="body-heading">
					<h3>Prerequisites</h3>
				</section>
				<section class="body-section">					
					<p>Before you begin, ensure you have:</p>
					<ol>
						<li>Git</li>
						<p>Check if git has installed. Open up command prompt as Admin</p>
						<pre>git --version</pre>
						<li>nodejs & npm</li>
						<p>Check if node has installed.</p>
						<pre>node --version</pre>
						<p>Check if npm has installed.</p>
						<pre>npm --version</pre>
						<li>Install webpack globally in your machine</li>
						<pre>npm install -g webpack</pre>
						<p class="alert alert-info">NOTE: Install globally allow us to execute webpack commands related</p>
					</ol>
				</section>
				<section class="body-heading">
					<h3>Steps to setup project folder:</h3>
				</section>
				<section class="body-section">
					<ol>
						<li>Clone repo in command prompt</li>
						<pre>git clone git@bitbucket.org:theokennygist/heroku-react.git</pre>
						<li>Change project name folder <code>"heroku-react"</code> to be something else</li>
						<p class="alert alert-info">
							NOTE: by default, the project name is <code>"heroku-react"</code>.
							Hence it is ideal to change project name folder.
						</p>
						<li>Delete existing git connection.</li>
						<pre>git remote rm origin</pre>
						<p class="alert alert-info">
							NOTE: Navigate into project folder. Execute command above. 
							Git-connection exist, because the clone process of Heroku-React repo 
						</p>
						<p class="alert alert-warning">
							Before remove connection. Check if the git-connection exist or not through command below:
							<code>git remote -v</code>
						</p>	
						<li>Run command as admin</li>
						<pre>npm install</pre>
						<p class="alert alert-info">NOTE: It will install all nodejs packages for Heroku-React</p>
																	
					</ol>
				</section>
				<section class="body-heading">
					<h3>Steps to run React</h3>
				</section>
				<section class="body-section">					
					<ol>
						<li>It bundle files and watch if there is a change</li>
						<pre>webpack -w</pre>						
						<li>Open up another command prompt as admin</li>
						<pre>node server.js</pre>
						<li>Open up the browser and type URL address:</li> 
						<pre>localhost:3000</pre>
						<p class="alert alert-info">
						NOTE:
							Heroku-React configures to use port 3000
							Modify server.js file to change local server setting
						</p>
					</ol>
				</section>
				<section class="body-heading">
					<h3>Steps to run test</h3>
				</section>			
				<section class="body-section">
					<ol>
						<p class="alert alert-warning">If webpack watch (webpack -w) is running, terminate it</p>
						<li>Open up command prompt as admin</li>
						<li>type command below:</li>
						<pre>npm test</pre>
					</ol>		
				</section>
			</div>
		)
	}
}