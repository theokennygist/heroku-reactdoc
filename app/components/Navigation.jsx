import React from 'react'
import {Link, IndexLink} from 'react-router'

export default class Navigation extends React.Component {
	render() {
		return (
			<nav class="navbar navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="#">Heroku-React</a>
					</div>
					<div class="navbar-collapse collapse" id="navbar">
						<ul class="nav navbar-nav">
							<li>
								<IndexLink to="/" class="top-link">Home</IndexLink>
							</li>	
							<li>
								<Link to="/gettingstarted" class="top-link">Getting Started</Link>
							</li>
							<li>
								<Link to="/packages" class="top-link">Packages</Link>
							</li>
							<li>
								<Link to="/configs" class="top-link">Configs</Link>
							</li>
							<li>
								<Link to="/structure" class="top-link">Structure</Link>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		)
	}
}